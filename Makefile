CC= gcc -Wall -c
lib1= segundos
lib2= dias
lib3= formato


all: bin/estatico bin/dinamico
	export LD_LIBRARY_PATH=./librerias:$LD_LIBRARY_PATH
####################################################################################
##################### STATICO ######################################################
####################################################################################

bin/estatico: obj/main.o librerias/libestatic_lib.a
	mkdir -p bin/
	gcc -static $< -lestatic_lib -L ./librerias/ -o $@

librerias/libestatic_lib.a: obj/$(lib1).o obj/$(lib2).o obj/$(lib3).o
	mkdir -p librerias/ 
	ar rcs librerias/libestatic_lib.a $^

##################### Objects files para link STATICO ##############################

obj/main.o: src/main.c
	mkdir -p obj/
	$(CC) -I head/ $^ -o $@

obj/$(lib1).o: src/$(lib1).c
	$(CC) $^ -o $@

obj/$(lib2).o: src/$(lib2).c
	$(CC) $^ -o $@

obj/$(lib3).o: src/$(lib3).c
	$(CC) $^ -o $@

####################################################################################
###################### DINAMICO ####################################################
####################################################################################

bin/dinamico: obj/main.o librerias/libdynamic.so
	gcc $^ ./librerias/libdynamic.so -o $@

librerias/libdynamic.so: src/$(lib1).c src/$(lib2).c src/$(lib3).c
	gcc -Wall -shared -fPIC $^ -o $@

####################################################################################
########################## CLEAR ###################################################
####################################################################################

.PHONY: clean
clean:
	rm bin/ -rf  
	rm obj/ -rf 
	rm librerias/ -rf