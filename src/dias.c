#include<stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
// "2020-10-30 10:30:52"

void diasToAMD (int dias)
{
    int years, months, days;
    // dias/365 son los numeros de años
    years= dias/365;
    // dias%365 son los dias en 1 año
    months = (dias%365)/30;
    // (dias%365)%30 son los dias en 1 mes
    days= (dias%365)%30 ; 
    printf("años\tmeses\tdias\n");
    printf("%d\t%d\t%d\t\n", years, months, days);
}