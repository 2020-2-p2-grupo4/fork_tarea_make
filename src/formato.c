#include<stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
// "2020-10-30 10:30:52"

void ISO_TosStr (char *arr)
{
    //const char *dateStr = "2020-10-30 10:30:52";
    char *mes;
    int y,M,d,h,m,s;
    sscanf(arr, "%d-%d-%d %d:%d:%d", &y, &M, &d, &h, &m, &s);
    switch(M){
        case 1:
            mes= "Enero";
            break;
        case 2:
            mes= "Febrero";
            break;
        case 3:
            mes= "Marzo";
            break;
        case 4:
            mes= "Abril";
            break;
        case 5:
            mes= "Mayo";
            break;
        case 6:
            mes= "Junio";
            break;
        case 7:
            mes= "Julio";
            break;
        case 8:
            mes= "Agosto";
            break;
        case 9:
            mes= "Septiembre";
            break;
        case 10:
            mes= "Octubre";
            break;
        case 11:
            mes= "Noviembre";
            break;
        case 12:
            mes= "Diciembre";
            break;
    }
    printf("%d de %s de %d %d:%d:%d\n", d, mes,y, h, m, s);
}