#include<stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include "utilfecha.h"
// "2020-10-30 10:30:52"


int main (int argc , char ** argv){
    int option;
    while((option= getopt (argc, argv, "s:d:f:")) != -1){
        switch(option){
            case 's':
                printf("s: %s\n" , optarg);
                segToHMS (atoi(optarg));
                break;
            case 'd':
                diasToAMD(atoi(optarg));
                break;
            case 'f':
                ISO_TosStr ((char *) optarg);
                break;
            case '?':
                printf ( "opcion desconocida\n" );
                return 1;
            default:
                abort();
        }
    }

    if (argc == 1)
    {
        printf("Opciones de ejecucion:\n\n");
        printf("1) bin/(estatico o dinamico) -f \"2020-10-30 10:30:52\"\n");
        printf("2) bin/(estatico o dinamico) -d 7999\n");
        printf("3) bin/(estatico o dinamico) -s 7999\n");
    }
    return 1;
}