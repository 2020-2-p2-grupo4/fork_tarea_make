#include<stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
// "2020-10-30 10:30:52"

void segToHMS (int segundos)
{
    int hours, mins, secs;
    // segundos/3600 son los numeros de horas
    hours= segundos/3600;
    // segundos%3600 son los minutos en una hora
    mins = (segundos%3600)/60;
    // (segundos%3600)%60 son los segundos en un minuto
    secs= (segundos%3600)%60 ; 
    printf("horas\tminutos\tsegundos\n");
    printf("%d\t%d\t%d\t\n", hours, mins, secs);
}